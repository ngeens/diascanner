#! /usr/bin/env bash

# Dependencies
# ############
# - gphoto2
# - ...

# Settings
# ########
SLIDES=50 # default number of slides in a tray
SLIDE=1 # default start slide number
OUTPUT_PATH="/tmp/test" # default output dir (if none is given)
GPIO_PIN=5 # GPIO pin that controls button for slide advance
PUSH_DELAY=0.2 # how long to press down the button on the remote (in seconds)
SLIDE_DELAY=1 # how long does it takes to advance a slide (in seconds)
PAUSE_EXEC="n" # default value for 
# CAPTURE_DELAY=1 # waiting while capturing (in seconds)

# Functions
# #########

advance_slide () {
    # echo "Send '1' to /sys/class/gpio/gpio${GPIO_PIN}/value"
    echo "1" > /sys/class/gpio/gpio${GPIO_PIN}/value
    sleep $PUSH_DELAY
    # echo "Send '0' to /sys/class/gpio/gpio${GPIO_PIN}/value"
    echo "0" > /sys/class/gpio/gpio${GPIO_PIN}/value
    sleep $SLIDE_DELAY
}

capture () {
    echo "Writing to ${OUTPUT_PATH}/${BASE_DIRNAME}-slide-${SLIDE}.jpg"
    gphoto2 --capture-image-and-download --filename ${OUTPUT_PATH}/${BASE_DIRNAME}-slide-${SLIDE}.jpg
    # sleep $CAPTURE_DELAY
}

ctrl_c () { # function to trap CTRL-C
    echo "** Trapped CTRL-C **"
    GPIO_setup unexport # Perform clean up
    kill -term $$ # Commit suicide.  Bye.
}

GPIO_setup () {
  action="$1"
  if [[ $action = "export" ]]
    then
      # Set up GPIO and set to output
      sudo echo $GPIO_PIN > /sys/class/gpio/export
      echo "out" > /sys/class/gpio/gpio${GPIO_PIN}/direction
      echo "GPIO exported"
  elif [[ $action =  "unexport" ]]
    then
      # GPIO pin unexport (clean up)
      echo $GPIO_PIN > /sys/class/gpio/unexport
      echo "GPIO unexported"
  else
      echo "GPIO error: action not recognized..."
  fi
}

# Preparing the stage
# ###################

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

# check wether output folder is given as $1
if [ ! -z "$1" ]
  then
    OUTPUT_PATH=$1
    echo $OUTPUT_PATH "given as output path"
  else
    read -ep "Output folder [${OUTPUT_PATH}]: " INPUT
    OUTPUT_PATH=${INPUT:-${OUTPUT_PATH}}
fi

# check wether output folder already exists
# if not, create it
if [ ! -d "$OUTPUT_PATH" ] 
  then
    mkdir -m 777 $OUTPUT_PATH
    echo "Created..."
  else
    echo "[Warning] Folder already exists, existing files inside will be overwritten!"
fi

# set basedir name to innclude in path & filename later...
BASE_DIRNAME="$(basename "$OUTPUT_PATH")" #extract basedir from path (to include in filename)


# check wether different number of slides is provided as $2
# && that it is an integer
if [ ! -z "$2" ] && [ $2 -eq $2 ]
  then
    SLIDES=$2
  else
    read -ep "Number of slides [${SLIDES:-1}]: " INPUT
    SLIDES=${INPUT:-${SLIDES}}
fi

# start slide numbering at
read -ep "Start at slide [${SLIDE:-1}]: " INPUT
SLIDE=${INPUT:-${SLIDE}}

# review paused execution
read -ep "Pause between slides [${PAUSE_EXEC:-1}]: " INPUT
PAUSE_EXEC=${INPUT:-${PAUSE_EXEC}}


# Pause to review settings
# ########################

echo "[REVIEW]" $SLIDES "slide(s) to scan into" $OUTPUT_PATH    
read -sn 1 -p "Press any key to start scan routine. (Ctrl-C to quit)" 
echo -e "\n"


# Perform actual scan routine
# ###########################

GPIO_setup export

while [ "$SLIDE" -le "$SLIDES" ]
  do
    # echo "Capturing to" $OUTPUT_PATH
    capture
    echo "Slide" $SLIDE "of" $SLIDES "..."
    advance_slide
    SLIDE=$(($SLIDE + 1)) # increase slide counter
    if [[ $PAUSE_EXEC = "y" ]]
      then
        read -sn 1 -p "Paused. Press any key to continue. (Ctrl-C to quit)" 
        echo -e "\n"
    fi
done

echo "Scan routine finished!"

# Clean up
# ########

GPIO_setup unexport

exit 0
